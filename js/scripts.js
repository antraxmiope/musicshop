$(document).ready(function () {

    checkSize();
    $(window).resize(checkSize);

    $('.product-carousel').slick({
        variableWidth: true,
        arrows: false,
        dots: true,
        swipeToSlide: true,
        mobileFirst: true,
        responsive: [{
            breakpoint: 200,
            settings: {
                centerMode: true
            }
        }, {
            breakpoint: 1024,
            settings: {
                centerMode: false
            }
        }]
    });
    $('.review-carousel').slick({
        variableWidth: true,
        arrows: false,
        dots: true,
        swipeToSlide: true,
        mobileFirst: true,
        responsive: [{
            breakpoint: 200,
            settings: {
                centerMode: true
            }
        }, {
            breakpoint: 1024,
            settings: {
                centerMode: false
            }
        }]
    });

    clampIt(document.getElementsByClassName('card-artist'), 3);
    clampIt(document.getElementsByClassName('card-album'), 4);
    clampIt(document.getElementsByClassName('review-card-artist'), 3);
    clampIt(document.getElementsByClassName('review-card-subtitle'), 5);
    clampIt(document.getElementsByClassName('review-album-title'), 6);
    clampIt(document.getElementsByClassName('review-album-artist'), 3);

    $("#tracklistToggler").click(function () {
        $("#tracklistTogglerRow").hide();
    });

    $('#add-review-modal').on('shown.bs.modal', function () {
        $('.empty-stars').popover({
            delay: {
                "show": 0,
                "hide": 2000
            },
            placement: "top",
            content: "Don't forget to rate!",
        })
        $('.empty-stars').popover('toggle');
        $('.empty-stars').popover('toggle');
    })
});

function clampIt(items, nOfLines) {
    for (var i = 0; i < items.length; i++) {
        $clamp(items.item(i), {
            clamp: nOfLines,
            useNativeClamp: false
        });
    }
}

function checkSize() {
    if ($("#navbar").css("display") == "none") {
        $('#filters-column').removeClass();
        $('#filters-panel').addClass('modal fade');
        $('#filters-dialog').addClass('modal-dialog modal-sm');
        $('#filters-body').addClass('modal-body');
        $('#filters-content').addClass('modal-content');
        $('#nav-search').removeClass("input-group-sm");
    } else {
        $('#filters-column').addClass('col-3 filters-right-border');
        $('#filters-panel').removeClass();
        $('#filters-panel').show();
        $('#filters-dialog').removeClass();
        $('#filters-body').removeClass();
        $('#filters-content').removeClass();
        $('#nav-search').addClass("input-group-sm");
    }
}